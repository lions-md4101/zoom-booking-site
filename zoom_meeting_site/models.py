from pydantic import BaseModel, HttpUrl

import datetime
from typing import Optional


class MeetingTime(BaseModel, arbitrary_types_allowed=True):
    date: datetime.date
    time: datetime.time
    duration: int


class Requester(BaseModel):
    name: str
    # email is deliberately str not a validated email field as the value need not be set
    email: str


class ZoomMeeting(BaseModel):
    id: int
    join_url: HttpUrl
    passcode: Optional[int]


class Meeting(BaseModel):
    meeting_time: MeetingTime
    zoom_meeting: ZoomMeeting = None
    topic: str
    requester: Requester
