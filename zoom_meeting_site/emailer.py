import os

import models

from dotenv import load_dotenv
from jinja2 import Environment, FileSystemLoader, select_autoescape
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import (
    Mail,
    MimeType,
    Content,
    Bcc,
)

load_dotenv()

MAILER = SendGridAPIClient(os.getenv("SENDGRID_API_KEY"))
BCCS = [a for a in os.getenv("EMAIL_BCCS").split(";") if a]

FROM_MAIL = (os.getenv("EMAIL_SENDER"), "Lions District 410E Zoom Bookings")

JINJA_ENV = Environment(
    loader=FileSystemLoader("email_templates"),
    autoescape=select_autoescape(),
)

TEMPLATE = JINJA_ENV.get_template("email.txt")

ZOOM_USERNAME = os.getenv("ZOOM_USERNAME")
ZOOM_PASSWORD = os.getenv("ZOOM_PASSWORD")


def send_mail(
    meeting: models.Meeting,
    bccs=BCCS,
):
    recipients = [(meeting.requester.email, meeting.requester.email)]

    body = TEMPLATE.render(
        {
            "topic": meeting.topic,
            "time": meeting.meeting_time.time.strftime("%H:%M"),
            "date": meeting.meeting_time.date.strftime("%d %B %Y"),
            "join_url": meeting.zoom_meeting.join_url,
            "passcode": meeting.zoom_meeting.passcode,
            "username": os.getenv("ZOOM_USERNAME"),
            "password": os.getenv("ZOOM_PASSWORD"),
        }
    )
    print(body)
    message = Mail(
        from_email=FROM_MAIL,
        to_emails=recipients,
        subject=f"Zoom booking for {meeting.meeting_time.time:%H:%M} on {meeting.meeting_time.date:%Y-%m-%d}",
    )
    if bccs:
        message.bcc = [Bcc(bcc, bcc, p=0) for bcc in bccs]
    message.content = Content(MimeType.html, body)
    response = MAILER.send(message)


if __name__ == "__main__":
    import datetime
    import meetings

    dt = datetime.datetime.now()
    m = meetings.make_meeting(
        topic="Test Meeting",
        requester_name="Kim van Wyk",
        requester_email="vanwykk@gmail.com",
        start_datetime=dt,
        end_datetime=dt + datetime.timedelta(hours=2),
    )
    send_mail(m)
