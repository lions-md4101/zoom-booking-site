from flask_bootstrap import Bootstrap5
from flask import Flask, render_template, redirect, url_for, session
import pymongo

import db
import emailer
import forms
import meetings
import services

import datetime
import os

app = services.init_app(Flask(__name__))
services.register_factory(app, pymongo.collection, db.make_collection)
app.secret_key = os.getenv("FLASK_SECRET_KEY")

bootstrap = Bootstrap5(app)

ZOOM_USERNAME = os.getenv("ZOOM_USERNAME")
ZOOM_PASSWORD = os.getenv("ZOOM_PASSWORD")


@app.route("/")
def index():
    m = meetings.retrieve_categorised_meetings()
    return render_template(
        "current.html", meetings=m, now=datetime.datetime.now().strftime("%d %b %Y")
    )


@app.route("/book/start", methods=("GET", "POST"))
def book_start():
    form = forms.DateForm()
    if form.validate_on_submit():
        date = form.date.data
        session["date"] = date.strftime("%Y-%m-%d")
        return redirect(url_for("book_time"))
    return render_template(
        "form.html",
        form=form,
        title="Select date",
        action=url_for("book_start"),
        back=url_for("index"),
    )


@app.route("/book/time", methods=("GET", "POST"))
def book_time():
    date = session.get("date")
    if date is None:
        return redirect(url_for("book_start"))
    date = datetime.datetime.strptime(date, "%Y-%m-%d")
    unavailable_times = []
    for m in meetings.retrieve_meetings_for_date(date):
        tr = meetings.get_meeting_time_range(m.meeting_time)
        unavailable_times.append(tr)
    if unavailable_times:
        description = f"These timeslots are not available: {';'.join([meetings.get_time_range_string(t) for t in unavailable_times])}"
    else:
        description = ""
    form = forms.TimeForm()
    error = None
    if form.validate_on_submit():
        start_time = datetime.datetime.combine(date, form.start_time.data)
        end_time = datetime.datetime.combine(date, form.end_time.data)
        for tr in unavailable_times:
            if start_time in tr:
                error = f"The selected start time of {start_time:%H:%M} is inside the existing {meetings.get_time_range_string(tr)} booking. Please select a different time."
                break
        if end_time <= start_time:
            error = "The selected end time is earlier than the start time. Please select a later end time."
        if error is None:
            session["start_time"] = start_time.strftime("%H:%M")
            session["end_time"] = end_time.strftime("%H:%M")
            return redirect(url_for("book_details"))
    return render_template(
        "form.html",
        form=form,
        title="Select times",
        action=url_for("book_time"),
        back=url_for("book_start"),
        description=description,
        error=error,
    )


@app.route("/book/details", methods=("GET", "POST"))
def book_details():
    date = session.get("date")
    if date is None:
        return redirect(url_for("book_start"))
    date = datetime.datetime.strptime(date, "%Y-%m-%d").date()

    start_time = session.get("start_time")
    end_time = session.get("end_time")
    if start_time is None or end_time is None:
        return redirect(url_for("book_time"))
    start_datetime = datetime.datetime.combine(
        date, datetime.datetime.strptime(start_time, "%H:%M").time()
    )
    end_datetime = datetime.datetime.combine(
        date, datetime.datetime.strptime(end_time, "%H:%M").time()
    )
    form = forms.DetailsForm()
    if form.validate_on_submit():
        meeting = meetings.make_meeting(
            form.topic.data,
            form.name.data,
            form.email.data,
            start_datetime,
            end_datetime,
        )
        emailer.send_mail(meeting)
        return render_template(
            "meeting_details.html",
            form=form,
            meeting=meeting,
            zoom_username=ZOOM_USERNAME,
            zoom_password=ZOOM_PASSWORD,
        )
    return render_template(
        "form.html",
        form=form,
        title=f"Finalise booking for {start_datetime:%H:%M}-{end_datetime:%H:%M} {start_datetime:%Y-%m-%d}",
        action=url_for("book_details"),
        back=url_for("book_time"),
    )
