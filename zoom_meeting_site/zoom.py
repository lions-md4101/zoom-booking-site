import models

import datetime
import os
from random import randint

import arrow
from dotenv import load_dotenv
import httpx
from rich import print

load_dotenv()

AUTH_HEADER = {"Host": "zoom.us"}
AUTH_DATA = {
    "grant_type": "account_credentials",
    "account_id": os.getenv("ZOOM_ACCOUNT_ID"),
}
AUTH_CREDENTIALS = (os.getenv("ZOOM_CLIENT_ID"), os.getenv("ZOOM_CLIENT_SECRET"))

DEFAULTS = {
    "type": 2,
    "timezone": None,
    "settings": {
        "host_video": False,
        "participant_video": False,
        "join_before_host": True,
        "jbh_time": 0,
        "mute_upon_entry": True,
        "auto_recording": "none",
        "meeting_authentication": False,
    },
}


def get_token():
    res = httpx.post(
        "https://zoom.us/oauth/token",
        data=AUTH_DATA,
        headers=AUTH_HEADER,
        auth=AUTH_CREDENTIALS,
    )
    return res.json()["access_token"]


def get_auth_headers():
    token = get_token()
    return {
        "content-type": "application/json",
        "authorization": f"Bearer {token}",
    }


def list_meetings(start, end):
    auth_headers = get_auth_headers()
    start = start.strftime("%Y-%m-%d")
    end = end.strftime("%Y-%m-%d")
    res = httpx.get(
        f"https://api.zoom.us/v2/users/me/meetings",
        headers=auth_headers,
        params={"page_size": 300, "from": start, "to": end},
    )
    res.raise_for_status()

    meetings = []
    for meeting in res.json()["meetings"]:
        dt = arrow.get(meeting["start_time"]).to(meeting["timezone"]).datetime
        mt = models.MeetingTime(
            date=dt.date(),
            time=dt.time(),
            duration=int(meeting["duration"]),
        )
        if "agenda" not in meeting:
            (name, email) = ("", "")
        else:
            (name, email) = (
                meeting["agenda"].split(":", 1)[-1].strip().split("(Email: ")
            )
            name = name.strip()
            email = email[:-1]
        requester = models.Requester(name=name, email=email)
        zm = models.ZoomMeeting(
            id=meeting["id"], join_url=meeting["join_url"], passcode=None
        )
        meet = models.Meeting(
            meeting_time=mt,
            zoom_meeting=zm,
            topic=meeting["topic"],
            requester=requester,
        )
        meetings.append(meet)
    return meetings


def make_meeting(meeting: models.Meeting):
    auth_headers = get_auth_headers()

    passcode = randint(100000, 999999)
    data = DEFAULTS.copy()
    data["topic"] = meeting.topic
    data["start_time"] = datetime.datetime.combine(
        meeting.meeting_time.date, meeting.meeting_time.time
    ).strftime("YYYY-MM-DDTHH:mm:ss")
    data["duration"] = meeting.meeting_time.duration
    data[
        "agenda"
    ] = f"Requester: {meeting.requester.name} (Email: {meeting.requester.email})"
    data["password"] = passcode
    res = httpx.post(
        f"https://api.zoom.us/v2/users/me/meetings",
        json=data,
        headers=auth_headers,
    )
    res.raise_for_status()

    json = res.json()
    meeting.zoom_meeting = models.ZoomMeeting(
        id=json["id"], join_url=json["join_url"], passcode=passcode
    )
    return meeting


if __name__ == "__main__":
    if 0:
        meetings = list_meetings(
            datetime.datetime.now() - datetime.timedelta(weeks=4),
            datetime.datetime.now() + datetime.timedelta(weeks=52),
        )
        print(meetings)

    if 1:
        dt = datetime.datetime.now()
        meeting = models.Meeting(
            meeting_time=models.MeetingTime(
                date=dt.date(), time=dt.time(), duration=120
            ),
            topic="Test Meeting",
            requester=models.Requester(name="Kim van Wyk", email="vanwykk@gmail.com"),
        )
        meeting = make_meeting(meeting)
        print(meeting)
