from flask_wtf import FlaskForm
from wtforms import DateField, EmailField, SubmitField, TimeField, StringField

# from wtforms.validators import DataRequired, NumberRange


class DateForm(FlaskForm):
    date = DateField(
        "Zoom booking date",
    )
    submit = SubmitField("Next")


class TimeForm(FlaskForm):
    start_time = TimeField(
        "Zoom booking start time",
    )
    end_time = TimeField(
        "Zoom booking end time",
    )
    submit = SubmitField("Next")


class DetailsForm(FlaskForm):
    topic = StringField(
        "Topic of the meeting",
        description="e.g. North Durban Lions club meeting. Please include your club name for club bookings and District name for District bookings",
        render_kw={"size": 50},
    )
    name = StringField(
        description="Name of Lion booking the meeting", render_kw={"size": 50}
    )
    email = EmailField(
        "Email address for Lion booking the meeting",
        description="This email address will only be used to email the details of the booking",
        render_kw={"size": 50},
    )
    submit = SubmitField("Book Meeting")
