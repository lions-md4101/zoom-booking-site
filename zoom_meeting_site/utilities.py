import typer

import db
import zoom

import datetime

app = typer.Typer()
reload_app = typer.Typer()
app.add_typer(reload_app, name="reload-meetings")


@reload_app.command()
def reload_meetings():
    collection = db.make_collection()
    db.clear_collection(collection)
    dt = datetime.datetime.now()
    for meeting in zoom.list_meetings(dt, dt + datetime.timedelta(days=3650)):
        db.write_meeting(meeting, collection)


if __name__ == "__main__":
    app()
