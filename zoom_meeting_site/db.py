from dotenv import load_dotenv
import pymongo
from rich import print

import datetime
import json
import os

import models

load_dotenv()


def make_client():
    return pymongo.MongoClient(
        host=os.getenv("MONGODB_HOST"),
        port=int(os.getenv("MONGODB_PORT")),
        username=os.getenv("MONGODB_USERNAME"),
        password=os.getenv("MONGODB_PASSWORD"),
    )


def make_collection():
    client = make_client()
    return client[os.getenv("MONGODB_DATABASE")][os.getenv("MONGODB_COLLECTION")]


def clear_collection(mongo_collection: pymongo.collection):
    mongo_collection.delete_many({})


def write_meeting(
    meeting: models.Meeting, mongo_collection: pymongo.collection, debug=False
):
    j = meeting.model_dump_json()
    if debug:
        print(j)
    d = json.loads(j)
    d["_id"] = d["zoom_meeting"]["id"]
    try:
        mongo_collection.insert_one(d)
    except pymongo.errors.DuplicateKeyError:
        pass


def read_meetings(
    mongo_collection: pymongo.collection,
    only_future: bool = False,
    date: datetime.date | str = None,
    debug=False,
):
    filter = {}
    if only_future:
        filter = {
            "meeting_time.date": {"$gte": datetime.date.today().strftime("%Y-%m-%d")}
        }
    elif date:
        if hasattr(date, "strftime"):
            date = date.strftime("%Y-%m-%d")
        filter = {
            "meeting_time.date": date,
        }
    l = mongo_collection.find(filter)
    meetings = []
    for d in l:
        del d["_id"]
        meeting = models.Meeting(**d)
        if debug:
            print(meeting)
        meetings.append(meeting)
    return meetings


if __name__ == "__main__":
    import zoom

    client = make_client()

    if 0:
        meetings = zoom.list_meetings(
            datetime.datetime.now() - datetime.timedelta(weeks=4),
            datetime.datetime.now() + datetime.timedelta(weeks=52),
        )
        for meeting in meetings:
            write_meeting(meeting, client["zoom_meeting"]["zoom_meeting"])

    if 0:
        read_meetings(client["zoom_meeting"]["zoom_meeting"], debug=True)

    if 1:
        clear_collection(client["zoom_meeting"]["zoom_meeting"])
