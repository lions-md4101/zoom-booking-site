from datetimerange import DateTimeRange
import pymongo

import db
import models
import services
import zoom

import collections
import datetime
from typing import List


def get_meeting_time_range(meeting_time: models.MeetingTime):
    te = datetime.datetime.combine(
        meeting_time.date, meeting_time.time
    ) + datetime.timedelta(minutes=meeting_time.duration)
    return DateTimeRange(
        datetime.datetime.combine(meeting_time.date, meeting_time.time), te
    )


def get_time_range_string(time_range: DateTimeRange):
    times = []
    for attr in ("start_datetime", "end_datetime"):
        times.append(getattr(time_range, attr).strftime("%H:%M"))
    return f"{times[0]} - {times[1]}"


def categorise_meeting_dates(meetings: List[models.Meeting]):
    d = collections.defaultdict(list)
    for meeting in meetings:
        d[meeting.meeting_time.date].append(
            get_meeting_time_range(meeting.meeting_time)
        )
    for v in d.values():
        v.sort(key=lambda x: x.start_datetime)
    keys = list(d.keys())
    keys.sort()
    return {
        k.strftime("%d %b %Y"): [get_time_range_string(v) for v in d[k]] for k in keys
    }


def retrieve_categorised_meetings():
    meetings = db.read_meetings(services.get(pymongo.collection), only_future=True)
    return categorise_meeting_dates(meetings)


def retrieve_meetings_for_date(date: datetime.date | str):
    return db.read_meetings(services.get(pymongo.collection), date=date)


def make_meeting(
    topic: str,
    requester_name: str,
    requester_email: str,
    start_datetime: datetime.datetime,
    end_datetime: datetime.datetime,
):
    mt = models.MeetingTime(
        date=start_datetime.date(),
        time=start_datetime.time(),
        duration=(end_datetime - start_datetime) / datetime.timedelta(minutes=1),
    )
    req = models.Requester(name=requester_name, email=requester_email)
    meeting = models.Meeting(meeting_time=mt, requester=req, topic=topic)
    m = zoom.make_meeting(meeting)
    db.write_meeting(m, services.get(pymongo.collection))
    return m


if __name__ == "__main__":
    dt = datetime.datetime.now()
    m = make_meeting(
        topic="Test Meeting",
        requester_name="Kim van Wyk",
        requester_email="vanwykk@gmail.com",
        start_datetime=dt,
        end_datetime=dt + datetime.timedelta(hours=2),
    )
    print(m)
